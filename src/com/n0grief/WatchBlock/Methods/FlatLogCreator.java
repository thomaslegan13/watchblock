package com.n0grief.WatchBlock.Methods;

import java.io.File;
import java.io.IOException;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import net.md_5.bungee.api.ChatColor;

public class FlatLogCreator {
    private static File file;
    private static FileConfiguration customFile;

    public static void setupblocksflatfile() {
        file = new File(Bukkit.getServer().getPluginManager().getPlugin("WatchBlock").getDataFolder(),"blocks.yml");
        if (!file.exists()) {
            try {
                file.createNewFile();
                Bukkit.getLogger().info(ChatColor.YELLOW + "[WATCHBLOCK] A NEW BLOCK-LOG FLATFILE WAS GENERATED.");
            } catch (IOException e) {
            }
        }
        customFile = YamlConfiguration.loadConfiguration(file);
        Bukkit.getLogger().info(ChatColor.GREEN + "[WATCHBLOCK] BLOCK-LOG FILE WAS SUCCESSFULLY LOADED! (THIS FILE WILL NOT USED IF USING MYSQL)");
        //FINDS/GENERATES THE BLOCKLOGGER FLATFILE
    }
    public static FileConfiguration get() {
        return customFile;
    }
    public static void save() {
        try {
            customFile.save(file);
        } catch (IOException e) {
            Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] WE WERE NOT ABLE TO SAVE YOUR FILE");
        }
    }

}
