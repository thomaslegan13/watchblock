package com.n0grief.WatchBlock.Tracking;

import com.n0grief.WatchBlock.SQL.MYSQL;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.SQLException;

public class TrackingKeepAlive extends BukkitRunnable {
    private final TrackerConnection trackerconnection;
    public TrackingKeepAlive(TrackerConnection trackerconnection){
        this.trackerconnection = trackerconnection;
    }
    @Override
    public void run() {
        if (trackerconnection.testConnection()) {
            //Bukkit.getLogger().info(ChatColor.GREEN + "[WATCHBLOCK] Connection to sql was checked: Connected");
        } else if (!trackerconnection.testConnection()) {
            Bukkit.getLogger().warning(ChatColor.DARK_RED + "[WATCHBLOCK] Connection to MotherShip was checked: Disconnected");
            Bukkit.getLogger().warning(ChatColor.DARK_RED + "[WATCHBLOCK] Attempting to reconnect to MotherShip...");
            Bukkit.broadcastMessage(ChatColor.YELLOW + "[WATCHBLOCK] MotherShip is attempting to reconnect, you may lag briefly...");
            try {
                trackerconnection.connect();
                Bukkit.getLogger().info(ChatColor.GREEN + "[WATCHBLOCK] SQLAutoCheck reconnected to MotherShip.");
                Bukkit.broadcastMessage(ChatColor.DARK_GREEN + "[WATCHBLOCK] MotherShip reconnected.");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                Bukkit.getLogger().warning(ChatColor.DARK_RED + "[WATCHBLOCK] Something went wrong, SQLAutoCheck couldn't reconnect to MotherShip.");
            } catch (SQLException e) {
                Bukkit.getLogger().warning(ChatColor.DARK_RED + "[WATCHBLOCK] Something went wrong, SQLAutoCheck couldn't reconnect to MotherShip.");
                e.printStackTrace();
            }
        }
    }
}
