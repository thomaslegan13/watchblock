package com.n0grief.WatchBlock.EventHandler;

import com.n0grief.WatchBlock.Main;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import java.util.Iterator;
import java.util.List;


public class OtherBlockEvents implements Listener {
    private Main plugin;
    private SQLLogger sqllogger;
    public OtherBlockEvents(Main plugin, SQLLogger sqllogger){
        this.sqllogger = sqllogger;
        this.plugin = plugin;
    }
    @EventHandler
    public void onentityExplosion(EntityExplodeEvent event){
        Entity entity = event.getEntity();
        List<Block> blockList = event.blockList();
        if(!(entity.getType() == EntityType.PRIMED_TNT)){
            Iterator<Block> iterator = blockList.iterator();
            while(iterator.hasNext()){
                Block block = iterator.next();
                Location b_loc = block.getLocation();
                int blockx = b_loc.getBlockX();
                int blocky = b_loc.getBlockY();
                int blockz = b_loc.getBlockZ();
                String world = b_loc.getWorld().getName();
                if(plugin.getConfig().getString("Allow-Creeper-Grief").equalsIgnoreCase("false")){
                    if(sqllogger.blockexists(blockx, blocky, blockz, world)){
                        iterator.remove();
                    }
                }
                else {
                    if (sqllogger.blockexists(blockx, blocky, blockz, world)) {
                        sqllogger.removeBlock(blockx, blocky, blockz, world);
                    }
                }
            }
        }
        else if(entity.getType() == EntityType.PRIMED_TNT){
            Iterator<Block> iterator = blockList.iterator();
            while(iterator.hasNext()){
                Block block = iterator.next();
                Location b_loc = block.getLocation();
                int blockx = b_loc.getBlockX();
                int blocky = b_loc.getBlockY();
                int blockz = b_loc.getBlockZ();
                String world = b_loc.getWorld().getName();
                if(sqllogger.blockexists(blockx, blocky, blockz, world)){
                    iterator.remove();
                }
            }
        }
    }
    @EventHandler
    public void onentityChange(EntityChangeBlockEvent event){
        Block block = event.getBlock();
        Location b_loc = block.getLocation();
        int blockx = b_loc.getBlockX();
        int blocky = b_loc.getBlockY();
        int blockz = b_loc.getBlockZ();
        String world = b_loc.getWorld().getName();
        if(plugin.getConfig().getString("Allow-Endermen-Grief").equalsIgnoreCase("false")) {
            if (sqllogger.blockexists(blockx, blocky, blockz, world)) {
                event.setCancelled(true);
            }
        }
    }
    @EventHandler
    public void onleavesDecay(LeavesDecayEvent event){
        Block block = event.getBlock();
        Location b_loc = block.getLocation();
        int blockx = b_loc.getBlockX();
        int blocky = b_loc.getBlockY();
        int blockz = b_loc.getBlockZ();
        String world = b_loc.getWorld().getName();
        if(plugin.getConfig().getString("Allow-Leaf-Decay").equalsIgnoreCase("false")) {
            if (sqllogger.blockexists(blockx, blocky, blockz, world)) {
                event.setCancelled(true);
            }
        }
    }
}
