package com.n0grief.WatchBlock.EventHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import com.n0grief.WatchBlock.Main;
import net.md_5.bungee.api.ChatColor;

public class SQLLogger implements Listener {
    private Main plugin;
    public SQLLogger(Main plugin) {
        this.plugin = plugin;
    }
    public boolean isFriend(int blockx, int blocky, int blockz, String world, Player player) {
        try {
            PreparedStatement ps5 = plugin.SQL.getConnection().prepareStatement("SELECT * FROM wb_blocks WHERE WORLD=? and X=? and Y=? and Z=?");
            ps5.setString(1, world);
            ps5.setInt(2, blockx);
            ps5.setInt(3, blocky);
            ps5.setInt(4, blockz);
            String breaker = player.getName();
            ResultSet results5 = ps5.executeQuery();
            while(results5.next()) {
                String blockowner = results5.getString("NAME");
                try {
                    PreparedStatement ps6 = plugin.SQL.getConnection().prepareStatement("SELECT * FROM wb_friends WHERE NAME=? and FRIENDS LIKE ?");
                    ps6.setString(1, blockowner);
                    ps6.setString(2, "%" + breaker + "%");
                    ResultSet results6 = ps6.executeQuery();
                    if(results6.next()) {
                        return true;
                    }
                } catch(SQLException e) {
                    e.printStackTrace();
                }
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public boolean isUnsupported(Material blocktype, Player player) {
        if(blocktype.equals(Material.TNT)
                || blocktype.equals(Material.FIRE)
                || blocktype.name().contains("ANVIL")
                || blocktype.equals(Material.SCAFFOLDING)
                || blocktype.name().contains("SIGN")
                || blocktype.name().endsWith("_SLAB")
                || blocktype.name().contains("SNOW")
                || blocktype.name().contains("ICE")
                || blocktype.name().endsWith("_SAPLING")
                || blocktype.name().contains("TORCH"))
        {
            if(plugin.getConfig().getString("WARN-OTHER-UNSUPPORTED").equalsIgnoreCase("true")){
                player.sendMessage(ChatColor.RED + "[WATCHBLOCK] Watchblock will not protect " + blocktype.name() + "!");
            }
            return true;
        }
        else if(blocktype.equals(Material.SAND)) {
            if(plugin.getConfig().getString("WARN-SAND").equalsIgnoreCase("true")) {
                player.sendMessage(ChatColor.RED + "[WATCHBLOCK] Watchblock will not protect sand as the physics it experiences are unsupported.");
            }
                return true;
        }
        else if(blocktype.equals(Material.GRAVEL)) {
            if(plugin.getConfig().getString("WARN-GRAVEL").equalsIgnoreCase("true")) {
                player.sendMessage(ChatColor.RED + "[WATCHBLOCK] Watchblock will not protect gravel as the physics it experiences are unsupported.");
            }
            return true;
        }
        else if(blocktype.equals(Material.WATER)
                || blocktype.equals(Material.WATER_BUCKET)) {
            if(plugin.getConfig().getString("WARN-WATER").equalsIgnoreCase("true")) {
                player.sendMessage(ChatColor.RED + "[WATCHBLOCK] Watchblock will not protect water as the physics it experiences are unsupported.");
            }
            return true;
        }
        else if(blocktype.equals(Material.LAVA)
                || blocktype.equals(Material.LAVA_BUCKET)) {
            if(plugin.getConfig().getString("WARN-LAVA").equalsIgnoreCase("true")) {
                player.sendMessage(ChatColor.RED + "[WATCHBLOCK] Watchblock will not protect lava as the physics it experiences are unsupported.");
            }
            return true;
        }
        else {
            return false;
        }

    }
    public void addBlock(int blockx, int blocky, int blockz, String world, Player player, UUID uuid){
        try {
            PreparedStatement ps2 = plugin.SQL.getConnection().prepareStatement("INSERT INTO wb_blocks" + "(NAME,UUID,WORLD,X,Y,Z) VALUES(?,?,?,?,?,?)");
            ps2.setString(1, player.getName());
            ps2.setString(2, uuid.toString());
            ps2.setString(3, world);
            ps2.setInt(4, blockx);
            ps2.setInt(5, blocky);
            ps2.setInt(6, blockz);
            ps2.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void removeBlock(int blockx, int blocky, int blockz, String world) {
        try {
            PreparedStatement ps5 = plugin.SQL.getConnection().prepareStatement("DELETE FROM wb_blocks WHERE WORLD=? and X=? and Y=? and Z=?");
            ps5.setString(1, world);
            ps5.setInt(2, blockx);
            ps5.setInt(3, blocky);
            ps5.setInt(4, blockz);
            ps5.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public boolean isOwner(int blockx, int blocky, int blockz, String world, Player player) {
        try {
            PreparedStatement ps4 = plugin.SQL.getConnection().prepareStatement("SELECT * FROM wb_blocks WHERE WORLD=? and X=? and Y=? and Z=?");
            ps4.setString(1, world);
            ps4.setInt(2, blockx);
            ps4.setInt(3, blocky);
            ps4.setInt(4, blockz);
            String breaker = player.getName();
            ResultSet results4 = ps4.executeQuery();
            while (results4.next()) {
                String blockowner = results4.getString("NAME");
                if(blockowner.equalsIgnoreCase(breaker)) {
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public String getOwner(int blockx, int blocky, int blockz, String world){
        try{
            PreparedStatement ps7 = plugin.SQL.getConnection().prepareStatement("SELECT * FROM wb_blocks WHERE WORLD=? and X=? and Y=? and Z=?");
            ps7.setString(1, world);
            ps7.setInt(2, blockx);
            ps7.setInt(3, blocky);
            ps7.setInt(4, blockz);
            ResultSet results7 = ps7.executeQuery();
            while(results7.next()){
                String owner = results7.getString("NAME");
                return owner;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public boolean blockexists(int blockx, int blocky, int blockz,String world) {
        try {
            PreparedStatement ps3 = plugin.SQL.getConnection().prepareStatement("SELECT * FROM wb_blocks WHERE WORLD=? and X=? and Y=? and Z=?");
            ps3.setString(1, world);
            ps3.setInt(2, blockx);
            ps3.setInt(3, blocky);
            ps3.setInt(4, blockz);
            ResultSet results1 = ps3.executeQuery();
            if(results1.next()) {
                return true;
            }
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        String playeruuid = event.getPlayer().getUniqueId().toString();
        Block block = event.getBlock();
        Player player = event.getPlayer();
        Location b_loc = block.getLocation();
        String world = b_loc.getWorld().getName();
        Integer blockx = b_loc.getBlockX();
        Integer blocky = b_loc.getBlockY();
        Integer blockz = b_loc.getBlockZ();
        if(!plugin.purgeblockArray.contains(playeruuid)
                && !plugin.blockinfoArray.contains(playeruuid)) {
            if (player.hasPermission("watchblock.bypass")) {
                removeBlock(blockx, blocky, blockz, world);
                if(block.getType().name().contains("_DOOR")){
                    if(player.getWorld().getBlockAt(blockx, blocky+1, blockz).getType().name().contains("_DOOR")){
                        removeBlock(blockx, blocky+1, blockz, world);
                    }
                    else if(player.getWorld().getBlockAt(blockx, blocky-1, blockz).getType().name().contains("_DOOR")){
                        removeBlock(blockx, blocky-1, blockz, world);
                    }
                }
                event.setCancelled(false);
            } else if (blockexists(blockx, blocky, blockz, world)) {
                if (isOwner(blockx, blocky, blockz, world, player)) {
                    event.setCancelled(false);
                    removeBlock(blockx, blocky, blockz, world);
                    if(block.getType().name().contains("_DOOR")){
                        if(player.getWorld().getBlockAt(blockx, blocky+1, blockz).getType().name().contains("_DOOR")){
                            removeBlock(blockx, blocky+1, blockz, world);
                        }
                        else if(player.getWorld().getBlockAt(blockx, blocky-1, blockz).getType().name().contains("_DOOR")){
                            removeBlock(blockx, blocky-1, blockz, world);
                        }
                    }
                } else if (!isOwner(blockx, blocky, blockz, world, player)) {
                    if (isFriend(blockx, blocky, blockz, world, player)) {
                        event.setCancelled(false);
                        removeBlock(blockx, blocky, blockz, world);
                        if(block.getType().name().contains("_DOOR")){
                            if(player.getWorld().getBlockAt(blockx, blocky+1, blockz).getType().name().contains("_DOOR")){
                                removeBlock(blockx, blocky+1, blockz, world);
                            }
                            else if(player.getWorld().getBlockAt(blockx, blocky-1, blockz).getType().name().contains("_DOOR")){
                                removeBlock(blockx, blocky-1, blockz, world);
                            }
                        }
                    } else {
                        String blockowner = getOwner(blockx, blocky, blockz, world);
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + "[WATCHBLOCK] " + blockowner + " owns that block!");
                    }
                }
            } else if (!blockexists(blockx, blocky, blockz, world)) {
                event.setCancelled(false);
            }
        }
    }
    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        String playeruuid = event.getPlayer().getUniqueId().toString();
        Block block = event.getBlock();
        Material blocktype = event.getBlock().getType();
        Player player = event.getPlayer();
        UUID uuid = player.getUniqueId();
        Location b_loc = block.getLocation();
        String world = b_loc.getWorld().getName();
        Integer blockx = b_loc.getBlockX();
        Integer blocky = b_loc.getBlockY();
        Integer blockz = b_loc.getBlockZ();
        if (!plugin.purgeblockArray.contains(playeruuid)
                && !plugin.blockinfoArray.contains(playeruuid)) {
            if(event.getBlockPlaced().getType().equals(Material.FARMLAND)
            || event.getBlockPlaced().getType().equals(Material.DIRT_PATH)
            || event.getBlockPlaced().getType().equals(Material.COARSE_DIRT)
            || event.getBlockPlaced().getType().equals(Material.WET_SPONGE)
            || event.getBlockPlaced().getType().equals(Material.CARVED_PUMPKIN)){
                if(!blockexists(blockx,blocky,blockz,world)){
                    addBlock(blockx, blocky, blockz, world, player, uuid);
                }
                else if(blockexists(blockx, blocky,blockz, world)){
                    if(!isOwner(blockx,blocky,blockz,world,player)){
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.RED + "[WATCHBLOCK] This block is owned by somebody else!");
                    }
                }
            }
            else if (isUnsupported(blocktype, player)) {
                event.setCancelled(false);
            }
            else if (blockexists(blockx, blocky, blockz, world)) {
                player.sendMessage(ChatColor.RED + "[WATCHBLOCK] The SQL server thinks this block is already registered to a player??? Possible SQL logging issue.");
                player.sendMessage(ChatColor.RED + "[WATCHBLOCK] If you see this error message the plugin may not be working right, contact server admin.");
                event.setCancelled(true);
            }
            else {
                event.setCancelled(false);
                addBlock(blockx,blocky,blockz,world,player,uuid);
            }
            if(block.getType().name().contains("_DOOR")){
                addBlock(blockx, blocky+1, blockz, world, player, uuid);
                player.sendMessage(ChatColor.GREEN + "[WATCHBLOCK] Door captured and protected.");
            }
        }
    }
}
