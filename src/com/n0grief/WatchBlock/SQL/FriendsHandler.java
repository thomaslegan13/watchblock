package com.n0grief.WatchBlock.SQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import com.n0grief.WatchBlock.Main;
import net.md_5.bungee.api.ChatColor;

public class FriendsHandler {
    private Main plugin;
    public FriendsHandler(Main plugin) {
        this.plugin = plugin;
    }
    public void createPlayer(Player player) {
        try {
            UUID uuid = player.getUniqueId();
            if(!exists(uuid)) {
                PreparedStatement ps2 = plugin.SQL.getConnection().prepareStatement("INSERT IGNORE INTO wb_friends"
                        + " (NAME,UUID) VALUES(?,?)");
                ps2.setString(1, player.getName());
                ps2.setString(2, uuid.toString());
                ps2.executeUpdate();
                Bukkit.getLogger().info(ChatColor.GREEN + "[WATCHBLOCK] NEW PLAYER: " + player.getName() + " has been added to the wb_friends table.");
                return;
            }
            if(exists(uuid)) {
                Bukkit.getLogger().info(ChatColor.GREEN + "[WATCHBLOCK] " + player.getName() + "'s SQL-based friends-list was found!");
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
    //This method checks to see if a player already exists in the database.
    public boolean exists(UUID uuid) {
        try {
            PreparedStatement ps = plugin.SQL.getConnection().prepareStatement("SELECT * FROM wb_friends WHERE UUID=?");
            ps.setString(1, uuid.toString());

            ResultSet results = ps.executeQuery();
            if(results.next()) {
                return true;
            }
            return false;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
