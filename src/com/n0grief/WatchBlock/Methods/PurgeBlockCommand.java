package com.n0grief.WatchBlock.Methods;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import com.n0grief.WatchBlock.Main;
import net.md_5.bungee.api.ChatColor;

public class PurgeBlockCommand implements CommandExecutor {
    private Main plugin;
    public PurgeBlockCommand(Main plugin) {
        this.plugin = plugin;
        plugin.getCommand("wpurgeblock").setExecutor(this);
    }
    public void wpurgeblockSQL(Player player, String world, String blockx, String blocky, String blockz) {
        try {
            PreparedStatement ps1 = plugin.SQL.getConnection().prepareStatement("DELETE FROM wb_blocks WHERE WORLD=? AND X=? AND Y=? AND Z=?");
            ps1.setString(1, world);
            ps1.setString(2, blockx);
            ps1.setString(3, blocky);
            ps1.setString(4, blockz);
            ps1.executeUpdate();
            player.sendMessage(ChatColor.GREEN + "[WATCHBLOCK] The block at coordinates: " + blockx + " " + blocky + " " + blockz + " from World: " + world + " has been purged from the MySQL database!");
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "[WATCHBLOCK] Only players in-game may execute this command!");
            return true;
        }
        Player player = (Player) sender;
        if (player.hasPermission("watchblock.admin")) {
            if(args.length == 4) {
                String world = args[0];
                String blockx = args[1];
                String blocky = args[2];
                String blockz = args[3];
                wpurgeblockSQL(player, world, blockx, blocky, blockz);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You have either specified too many or too few friends to remove. 1 block at a time please.");
            }
        }
        if(!player.hasPermission("watchblock.admin")) {
            player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You do not have permission to use that command!");
            Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK]" + player.getName() + " was denied access to " + cmd.getName());
        }
        return false;
    }
}
