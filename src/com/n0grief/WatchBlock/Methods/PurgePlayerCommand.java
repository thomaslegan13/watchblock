package com.n0grief.WatchBlock.Methods;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import com.n0grief.WatchBlock.Main;
import net.md_5.bungee.api.ChatColor;

public class PurgePlayerCommand implements CommandExecutor {
    private Main plugin;
    public PurgePlayerCommand(Main plugin) {
        this.plugin = plugin;
        plugin.getCommand("wpurgeplayer").setExecutor(this);
    }
    public void wpurgeplayerSQL(Player player, String args) {
        try {
            PreparedStatement ps1 = plugin.SQL.getConnection().prepareStatement("DELETE FROM wb_blocks WHERE NAME=?");
            ps1.setString(1, args);
            ps1.executeUpdate();
            player.sendMessage(ChatColor.GREEN + "[WATCHBLOCK] " + args + " has been purged from the MySQL database!");
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Only players in-game may execute this command!");
            return true;
        }
        Player player = (Player) sender;
        if (player.hasPermission("watchblock.admin")) {
            if(args.length == 1) {
                wpurgeplayerSQL(player, args[0]);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You have either specified too many or too few friends to remove. 1 player at a time please.");
                return false;
            }
        }
        if(!player.hasPermission("watchblock.admin")) {
            player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You do not have permission to use that command!");
            Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK]" + player.getName() + " was denied access to " + cmd.getName());
        }
        return false;
    }
}
