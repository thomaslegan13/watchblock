package com.n0grief.WatchBlock.Methods;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import com.n0grief.WatchBlock.Main;
import net.md_5.bungee.api.ChatColor;

public class PurgeWorldCommand implements CommandExecutor {
    private Main plugin;
    public PurgeWorldCommand(Main plugin) {
        this.plugin = plugin;
        plugin.getCommand("wpurgeworld").setExecutor(this);
    }
    public void wpurgeworldSQL(Player player, String world) {
        try {
            PreparedStatement ps1 = plugin.SQL.getConnection().prepareStatement("DELETE FROM wb_blocks WHERE WORLD=?");
            ps1.setString(1, world);
            ps1.executeUpdate();
            player.sendMessage(ChatColor.GREEN + "[WATCHBLOCK] All the blocks from World: " + world + " have been purged from the MySQL database.");
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Only players in-game may execute this command!");
            return true;
        }
        Player player = (Player) sender;
        if (player.hasPermission("watchblock.admin")) {
            if(args.length == 1) {
                String world = args[0];
                wpurgeworldSQL(player, world);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You have either specified too many or too few friends to remove. 1 world at a time please.");
            }
        }
        if(!player.hasPermission("watchblock.admin")) {
            player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You do not have permission to use that command!");
            Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK]" + player.getName() + " was denied access to " + cmd.getName());
        }
        return false;
    }
}
