package com.n0grief.WatchBlock.ToggleMethods;

import com.n0grief.WatchBlock.EventHandler.SQLLogger;
import com.n0grief.WatchBlock.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;

import java.util.ArrayList;

public class ToggleBlockInfo implements Listener, CommandExecutor {
    private Main plugin;
    private final SQLLogger sqllogger;
    public ToggleBlockInfo(Main plugin , SQLLogger sqllogger){
        this.plugin = plugin;
        this.sqllogger = sqllogger;
        plugin.getCommand("winfo").setExecutor(this);
    }


    @EventHandler
    public void onInteractEvent(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        String playeruuid = player.getUniqueId().toString();
        if (plugin.blockinfoArray.contains(playeruuid)) {
            Action action = event.getAction();
            if (action == Action.LEFT_CLICK_BLOCK) {
                Block block = event.getClickedBlock();
                Location b_loc = block.getLocation();
                Integer blockx = b_loc.getBlockX();
                Integer blocky = b_loc.getBlockY();
                Integer blockz = b_loc.getBlockZ();
                String world = b_loc.getWorld().getName();
                String blocktype = block.getType().toString();
                if (sqllogger.blockexists(blockx, blocky, blockz, world)) {
                    String owner = sqllogger.getOwner(blockx, blocky, blockz, world);
                    player.sendMessage("************************************************************");
                    player.sendMessage(ChatColor.GREEN + "[/WINFO] Protected: " + ChatColor.DARK_GREEN + "YES");
                    player.sendMessage(ChatColor.GREEN + "[/WINFO] Owner: " + owner);
                    player.sendMessage(ChatColor.GREEN + "[/WINFO] BlockType: " + blocktype);
                    player.sendMessage(ChatColor.GREEN + "[/WINFO] World: " + world);
                    player.sendMessage(ChatColor.GREEN + "[/WINFO] X: " + blockx);
                    player.sendMessage(ChatColor.GREEN + "[/WINFO] Y: " + blocky);
                    player.sendMessage(ChatColor.GREEN + "[/WINFO] Z: " + blockz);
                    player.sendMessage("************************************************************");
                    event.setCancelled(true);

                } else if (!sqllogger.blockexists(blockx, blocky, blockz, world)) {
                    player.sendMessage("************************************************************");
                    player.sendMessage(ChatColor.GREEN + "[/WINFO] Protected: " + ChatColor.DARK_RED + "NO");
                    player.sendMessage(ChatColor.GREEN + "[/WINFO] BlockType: " + blocktype);
                    player.sendMessage(ChatColor.GREEN + "[/WINFO] World: " + world);
                    player.sendMessage(ChatColor.GREEN + "[/WINFO] X: " + blockx);
                    player.sendMessage(ChatColor.GREEN + "[/WINFO] Y: " + blocky);
                    player.sendMessage(ChatColor.GREEN + "[/WINFO] Z: " + blockz);
                    player.sendMessage("************************************************************");
                    event.setCancelled(true);
                }
            } else if (action == Action.RIGHT_CLICK_BLOCK) {
                Block block = event.getClickedBlock();
                Location b_loc = block.getLocation();
                String world = b_loc.getWorld().getName();
                World targetworld = b_loc.getWorld();
                Integer blockxmod = event.getBlockFace().getModX();
                Integer blockymod = event.getBlockFace().getModY();
                Integer blockzmod = event.getBlockFace().getModZ();
                Integer blockx = blockxmod + b_loc.getBlockX();
                Integer blocky = blockymod + b_loc.getBlockY();
                Integer blockz = blockzmod + b_loc.getBlockZ();
                Integer unmodx = b_loc.getBlockX();
                Integer unmody = b_loc.getBlockY();
                Integer unmodz = b_loc.getBlockZ();
                Block targetblock = new Location(targetworld, blockx, blocky, blockz).getBlock();
                String blocktype = targetblock.getType().toString();
                if (event.getHand() == EquipmentSlot.HAND) {
                    if (!sqllogger.blockexists(blockx, blocky, blockz, world)) {
                        player.sendMessage("************************************************************");
                        player.sendMessage(ChatColor.GREEN + "[/WINFO] Protected: " + ChatColor.DARK_RED + "NO");
                        player.sendMessage(ChatColor.GREEN + "[/WINFO] BlockType: " + blocktype);
                        player.sendMessage(ChatColor.GREEN + "[/WINFO] World: " + world);
                        player.sendMessage(ChatColor.GREEN + "[/WINFO] X: " + blockx);
                        player.sendMessage(ChatColor.GREEN + "[/WINFO] Y: " + blocky);
                        player.sendMessage(ChatColor.GREEN + "[/WINFO] Z: " + blockz);
                        player.sendMessage("************************************************************");
                        event.setCancelled(true);
                    } else if (sqllogger.blockexists(unmodx, unmody, unmodz, world)) {
                        String owner = sqllogger.getOwner(blockx, blocky, blockz, world);
                        player.sendMessage("************************************************************");
                        player.sendMessage(ChatColor.GREEN + "[/WINFO] Protected: " + ChatColor.DARK_GREEN + "YES");
                        player.sendMessage(ChatColor.GREEN + "[/WINFO] Owner: " + owner);
                        player.sendMessage(ChatColor.GREEN + "[/WINFO] BlockType: " + blocktype);
                        player.sendMessage(ChatColor.GREEN + "[/WINFO] World: " + world);
                        player.sendMessage(ChatColor.GREEN + "[/WINFO] X: " + blockx);
                        player.sendMessage(ChatColor.GREEN + "[/WINFO] Y: " + blocky);
                        player.sendMessage(ChatColor.GREEN + "[/WINFO] Z: " + blockz);
                        player.sendMessage("************************************************************");
                        event.setCancelled(true);
                    } else if (event.getHand() == EquipmentSlot.OFF_HAND) {
                        if (!sqllogger.blockexists(blockx, blocky, blockz, world)) {
                            player.sendMessage("************************************************************");
                            player.sendMessage(ChatColor.GREEN + "[/WINFO] Protected: " + ChatColor.DARK_RED + "NO");
                            player.sendMessage(ChatColor.GREEN + "[/WINFO] BlockType: " + blocktype);
                            player.sendMessage(ChatColor.GREEN + "[/WINFO] World: " + world);
                            player.sendMessage(ChatColor.GREEN + "[/WINFO] X: " + blockx);
                            player.sendMessage(ChatColor.GREEN + "[/WINFO] Y: " + blocky);
                            player.sendMessage(ChatColor.GREEN + "[/WINFO] Z: " + blockz);
                            player.sendMessage("************************************************************");
                            event.setCancelled(true);
                        } else if (sqllogger.blockexists(blockx, blocky, blockz, world)) {
                            player.sendMessage("************************************************************");
                            player.sendMessage(ChatColor.GREEN + "[/WINFO] Protected: " + ChatColor.DARK_RED + "YES");
                            player.sendMessage(ChatColor.GREEN + "[/WINFO] BlockType: " + blocktype);
                            player.sendMessage(ChatColor.GREEN + "[/WINFO] World: " + world);
                            player.sendMessage(ChatColor.GREEN + "[/WINFO] X: " + blockx);
                            player.sendMessage(ChatColor.GREEN + "[/WINFO] Y: " + blocky);
                            player.sendMessage(ChatColor.GREEN + "[/WINFO] Z: " + blockz);
                            player.sendMessage("************************************************************");
                            event.setCancelled(true);
                        }
                    }
                }
            }
        }
    }


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        Player player = (Player) sender;
        String playeruuid = player.getUniqueId().toString();
        if(!(sender instanceof Player)){
            sender.sendMessage(ChatColor.RED + "[WATCHBLOCK] Only players in-game may execute this command!");
            return true;
        }
        if(player.hasPermission("watchblock.winfo")){
            if(args.length == 0){
                if(plugin.blockinfoArray.contains(playeruuid)
                        && !plugin.purgeblockArray.contains(playeruuid)
                        && !plugin.blockdebuggerArray.contains(playeruuid)){
                    plugin.blockinfoArray.remove(playeruuid);
                    player.sendMessage(ChatColor.DARK_GREEN + "[WATCHBLOCK] Block info disabled, repeat command to enable.");
                    Bukkit.getLogger().info(ChatColor.YELLOW + "[WATCHBLOCK] Block info disabled for " + player.getName());
                }
                else if(!plugin.blockinfoArray.contains(playeruuid)
                        && !plugin.purgeblockArray.contains(playeruuid)
                        && !plugin.blockdebuggerArray.contains(playeruuid)){
                    plugin.blockinfoArray.add(playeruuid);
                    player.sendMessage(ChatColor.DARK_GREEN + "[WATCHBLOCK] Block info enabled, repeat command to disable.");
                    Bukkit.getLogger().info(ChatColor.YELLOW + "[WATCHBLOCK] Block info disabled for " + player.getName());
                }
                if(plugin.purgeblockArray.contains(playeruuid)){
                    player.sendMessage(ChatColor.DARK_RED + "[WATCHBLOCK] Please disable /wpurge before enabling /winfo.");
                }
                if(plugin.blockdebuggerArray.contains(playeruuid)){
                    player.sendMessage(ChatColor.DARK_RED + "[WATCHBLOCK] Please disable /blockdebugger before enabling /winfo.");
                }
                return true;
            }
            else{
                player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You have specified too many arguments.");
                return false;
            }
        }
        if(!player.hasPermission("watchblock.info")){
            player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You do not have permission to use that command.");
            Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] " + player.getName() + " was denied access to " + cmd.getName());
            return true;
        }
        return false;
    }
}
