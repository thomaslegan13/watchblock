package com.n0grief.WatchBlock.SQL;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import com.n0grief.WatchBlock.Main;

public class TableCreator {
    private Main plugin;
    public TableCreator(Main plugin) {
        this.plugin = plugin;
    }
    public void createFriendsTable() {
        PreparedStatement ps;
        try {
            ps = plugin.SQL.getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS wb_friends " +
                    "(NAME VARCHAR(100),UUID VARCHAR(100),FRIENDS VARCHAR(10000) NOT NULL,PRIMARY KEY (NAME))");
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void createBlocksTable() {
        PreparedStatement ps;
        try {
            ps = plugin.SQL.getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS wb_blocks " +
                    "(ROWID INT(8) AUTO_INCREMENT,NAME VARCHAR(100),UUID VARCHAR(100),WORLD VARCHAR(100),X INT(8),Y INT(8),Z INT(8),PRIMARY KEY (ROWID))");
            ps.executeUpdate();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
