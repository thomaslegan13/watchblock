package com.n0grief.WatchBlock.Methods;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import com.n0grief.WatchBlock.Main;
import net.md_5.bungee.api.ChatColor;

public class PurgeFriendsCommand implements CommandExecutor {
    private Main plugin;
    public PurgeFriendsCommand(Main plugin) {
        this.plugin = plugin;
        plugin.getCommand("wpurgefriends").setExecutor(this);
    }
    public void wpurgefriendsSQL(Player player) {
        try {
            PreparedStatement ps1 = plugin.SQL.getConnection().prepareStatement("DELETE FROM wb_friends");
            ps1.executeUpdate();
            player.sendMessage(ChatColor.GREEN + "[WATCHBLOCK] The entire friendlist table has been purged MySQL database.");
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Only players in-game may execute this command!");
            return true;
        }
        Player player = (Player) sender;
        if (player.hasPermission("watchblock.admin")) {
            if(args.length == 0) {
                wpurgefriendsSQL(player);
                player.sendMessage(ChatColor.DARK_RED + "[WATCHBLOCK] WARNING: Please reboot server after purging the friends-list, otherwise you WILL experience weird bugs without even realizing it.");
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You included too many arguments in your command this command removes ALL friends for ALL players.");
            }
        }
        if(!player.hasPermission("watchblock.admin")) {
            player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You do not have permission to use that command!");
            Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK]" + player.getName() + " was denied access to " + cmd.getName());
        }
        return false;
    }
}
