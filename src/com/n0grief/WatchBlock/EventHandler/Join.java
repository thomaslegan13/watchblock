package com.n0grief.WatchBlock.EventHandler;

import com.n0grief.WatchBlock.Methods.UpdateChecker;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import com.n0grief.WatchBlock.SQL.FriendsHandler;

public class Join implements Listener {
    private final FriendsHandler friendshandler;
    private final UpdateChecker updatechecker;
    public Join(FriendsHandler friendshandler, UpdateChecker updatechecker) {
        this.friendshandler = friendshandler;
        this.updatechecker = updatechecker;
    }
    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        friendshandler.createPlayer(player);
        if(player.isOp()){
            if(!updatechecker.isupdatedSilent()){
                player.sendMessage(ChatColor.DARK_RED + "[WATCHBLOCK] Plugin out of date! Please update at https://www.spigotmc.org/resources/watchblock.96521/");
            }
        }
    }

}
