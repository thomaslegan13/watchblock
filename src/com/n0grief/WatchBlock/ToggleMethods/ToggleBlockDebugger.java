package com.n0grief.WatchBlock.ToggleMethods;

import com.n0grief.WatchBlock.Main;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.ArrayList;

public class ToggleBlockDebugger implements Listener , CommandExecutor {
    private Main plugin;
    public ToggleBlockDebugger(Main plugin) {
        this.plugin = plugin;
        plugin.getCommand("wblockdebugger").setExecutor(this);
    }


    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        String playeruuid = player.getUniqueId().toString();
        if(plugin.blockdebuggerArray.contains(playeruuid)){
            Block block = event.getBlock();
            Location b_loc = block.getLocation();

            player.sendMessage(ChatColor.GREEN + "[/wblockdebugger] You removed: " + block.getType());
            player.sendMessage(ChatColor.BLUE + "[/wblockdebugger] Location: ");
            player.sendMessage(ChatColor.GOLD + "[/wblockdebugger] World: " + b_loc.getWorld().getName());
            player.sendMessage(ChatColor.DARK_RED + "[/wblockdebugger] X:" + b_loc.getBlockX());
            player.sendMessage(ChatColor.DARK_RED + "[/wblockdebugger] Y:" + b_loc.getBlockY());
            player.sendMessage(ChatColor.DARK_RED + "[/wblockdebugger] Z:" + b_loc.getBlockZ());
        }
    }
    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        String playeruuid = player.getUniqueId().toString();
        if(plugin.blockdebuggerArray.contains(playeruuid)){
            Block block = event.getBlock();
            Location b_loc = block.getLocation();

            player.sendMessage(ChatColor.GREEN + "[/wblockdebugger] You placed: " + block.getType());
            player.sendMessage(ChatColor.BLUE + "[/wblockdebugger] Location: ");
            player.sendMessage(ChatColor.GOLD + "[/wblockdebugger] World: " + b_loc.getWorld().getName());
            player.sendMessage(ChatColor.DARK_RED + "[/wblockdebugger] X:" + b_loc.getBlockX());
            player.sendMessage(ChatColor.DARK_RED + "[/wblockdebugger] Y:" + b_loc.getBlockY());
            player.sendMessage(ChatColor.DARK_RED + "[/wblockdebugger] Z:" + b_loc.getBlockZ());
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        Player player = (Player) sender;
        String playeruuid = player.getUniqueId().toString();
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Only players in-game may execute this command!");
            return true;
        }
        if(player.hasPermission("watchblock.blockdebugger")){
            if(args.length == 0) {
                if(plugin.blockdebuggerArray.contains(playeruuid)
                        && !plugin.purgeblockArray.contains(playeruuid)
                        && !plugin.blockinfoArray.contains(playeruuid)){
                    plugin.blockdebuggerArray.remove(playeruuid);
                    player.sendMessage(ChatColor.DARK_GREEN + "[WATCHBLOCK] Block debugger disabled, repeat cmd to enable.");
                    Bukkit.getLogger().info(ChatColor.YELLOW + "[WATCBLOCK] Block debugger disabled for " + player.getName());
                }
                else if(!plugin.blockdebuggerArray.contains(playeruuid)
                        && !plugin.purgeblockArray.contains(playeruuid)
                        && !plugin.blockinfoArray.contains(playeruuid)){
                    plugin.blockdebuggerArray.add(playeruuid);
                    player.sendMessage(ChatColor.DARK_GREEN + "[WATCHBLOCK] Block debugger enabled, repeat cmd to disable.");
                    Bukkit.getLogger().info(ChatColor.YELLOW + "[WATCBLOCK] Block debugger enabled for " + player.getName());
                }
                if(plugin.purgeblockArray.contains(playeruuid)){
                    player.sendMessage(ChatColor.DARK_RED + "[WATCHBLOCK] Please disable /wpurge before enabling /blockdebugger.");
                }
                if(plugin.blockinfoArray.contains(playeruuid)){
                    player.sendMessage(ChatColor.DARK_RED + "[WATCHBLOCK] Please disable /winfo before enabling /wblockdebugger.");
                }
                return true;
            }
            else{
                player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You have specified too many arguments.");
                return false;
            }
        }
        if(!player.hasPermission("watchblock.blockdebugger")){
            player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You do not have permission to use that command!");
            Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] " + player.getName() + " was denied access to " + cmd.getName());
            return true;
        }
        return false;
    }
}
