package com.n0grief.WatchBlock.ToggleMethods;

import com.n0grief.WatchBlock.EventHandler.SQLLogger;
import com.n0grief.WatchBlock.Main;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.ArrayList;

public class TogglePurgeBlock implements Listener , CommandExecutor {
    private Main plugin;
    private final SQLLogger sqllogger;
    public TogglePurgeBlock(Main plugin , SQLLogger sqllogger){
        this.plugin = plugin;
        this.sqllogger = sqllogger;
        plugin.getCommand("wpurge").setExecutor(this);
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event){
        Player player = event.getPlayer();
        String playeruuid = player.getUniqueId().toString();
        if(plugin.purgeblockArray.contains(playeruuid)){
            Block block = event.getBlock();
            Location b_loc = block.getLocation();
            String world = b_loc.getWorld().getName();
            int blockx = b_loc.getBlockX();
            int blocky = b_loc.getBlockY();
            int blockz = b_loc.getBlockZ();
            if(!sqllogger.blockexists(blockx,blocky,blockz,world)){
                player.sendMessage(ChatColor.YELLOW + "[WATCHBLOCK] Block was not protected.");
            }
            else if(sqllogger.blockexists(blockx,blocky,blockz,world)){
                sqllogger.removeBlock(blockx,blocky,blockz,world);
                player.sendMessage(ChatColor.GREEN + "[WATCHBLOCK] Protection was removed!");
            }
            event.setCancelled(true);
        }
    }
    @EventHandler
    public void onPlace(BlockPlaceEvent event){
        Player player = event.getPlayer();
        String playeruuid = player.getUniqueId().toString();
        if(plugin.purgeblockArray.contains(playeruuid)){
            Block block = event.getBlock();
            Location b_loc = block.getLocation();
            String world = b_loc.getWorld().getName();
            int blockx = b_loc.getBlockX();
            int blocky = b_loc.getBlockY();
            int blockz = b_loc.getBlockZ();
            if(!sqllogger.blockexists(blockx,blocky,blockz,world)){
                player.sendMessage(ChatColor.YELLOW + "[WATCHBLOCK] Block was not protected.");
            }
            else if(sqllogger.blockexists(blockx,blocky,blockz,world)){
                sqllogger.removeBlock(blockx,blocky,blockz,world);
                player.sendMessage(ChatColor.GREEN + "[WATCHBLOCK] Protection was removed!");
            }
            event.setCancelled(true);
        }
    }


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        Player player = (Player) sender;
        String playeruuid = player.getUniqueId().toString();

        if(!(sender instanceof Player)){
            sender.sendMessage(ChatColor.RED + "Only players in-game may execute this command!");
            return true;
        }
        if(player.hasPermission("watchblock.admin")){
            if(args.length == 0){
                if(plugin.purgeblockArray.contains(playeruuid)
                        && !plugin.blockinfoArray.contains(playeruuid)
                        && !plugin.blockdebuggerArray.contains(playeruuid)){
                    plugin.purgeblockArray.remove(playeruuid);
                    player.sendMessage(ChatColor.DARK_GREEN + "[WATCHBLOCK] Block purger disabled, repeat cmd to enable.");
                    Bukkit.getLogger().info(ChatColor.YELLOW + "[WATCBLOCK] Block purger disabled for " + player.getName());
                }
                else if(!plugin.purgeblockArray.contains(playeruuid)
                        && !plugin.blockinfoArray.contains(playeruuid)
                        && !plugin.blockdebuggerArray.contains(playeruuid)){
                    plugin.purgeblockArray.add(playeruuid);
                    player.sendMessage(ChatColor.DARK_GREEN + "[WATCHBLOCK] Block purger enabled, repeat cmd to disable.");
                    Bukkit.getLogger().info(ChatColor.YELLOW + "[WATCBLOCK] Block purger enabled for " + player.getName());
                }
                if(plugin.blockinfoArray.contains(playeruuid)){
                    player.sendMessage(ChatColor.DARK_RED + "[WATCHBLOCK] Please disable /winfo before enabling /wpurge.");
                }
                if(plugin.blockdebuggerArray.contains(playeruuid)){
                    player.sendMessage(ChatColor.DARK_RED + "[WATCHBLOCK] Please disable /wblockdebugger before enabling /wpurge.");
                }
                return true;
            }
            else{
                player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You have specified too many arguments.");
                return false;
            }

        }
        if (!player.hasPermission("watchblock.admin")){
            player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You do not have permission to use that command!");
            Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] " + player.getName() + " was denied access to " + cmd.getName());
            return true;
        }
        return false;
    }
}
