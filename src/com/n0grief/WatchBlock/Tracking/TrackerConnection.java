package com.n0grief.WatchBlock.Tracking;

import com.n0grief.WatchBlock.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;
import java.sql.*;

public class TrackerConnection {

    //PULLING MYSQL LOGIN INFO AND DEFINING IT BASED OF CONTENT OF CONFIG.YML
    Plugin plugin = Main.getPlugin(Main.class);
    private String host = "n0grief.com";
    private String port = "3306";
    private String database = "mothershiplog";
    private String username = "influx";
    private String password = "placeholder";
    private Connection connection;

    public boolean isConnected() {
        return (connection == null ? false : true);
    }


    //IF SQL IS NOT CONNECTED THIS CODE IS RAN TO CONNECT TO IT
    public void connect() throws ClassNotFoundException, SQLException {
        connection = DriverManager.getConnection("jdbc:mysql://" +
                        host + ":" + port + "/" + database + "?useSSL=false" + "&autoReconnect=false",
                username, password);
    }
    //SQL DISCONNECTION CODE
    public void disconnect() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public Connection getConnection() {
        return connection;
    }

    public boolean testConnection(){
        try{
            PreparedStatement ps1 = getConnection().prepareStatement("SELECT * FROM servers");
            ResultSet results1 = ps1.executeQuery();
            if(results1.next()){
                return true;
            }
            else{
                Bukkit.getLogger().warning(ChatColor.DARK_RED + "[WATCHBLOCK] If you are seeing this message, something went very, very wrong.");
                return false;
            }
        }catch(SQLException e){
            Bukkit.getLogger().warning(ChatColor.DARK_RED + "[WATCHBLOCK] The SQL server was found to be disconnected.");
            return false;
        }
    }
}
