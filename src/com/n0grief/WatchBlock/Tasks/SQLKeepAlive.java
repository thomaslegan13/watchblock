package com.n0grief.WatchBlock.Tasks;

import com.n0grief.WatchBlock.SQL.MYSQL;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.SQLException;

public class SQLKeepAlive extends BukkitRunnable {
    private final MYSQL mysql;
    public SQLKeepAlive(MYSQL mysql){
        this.mysql = mysql;
    }
    @Override
    public void run() {
        if (mysql.testConnection()) {
            //Bukkit.getLogger().info(ChatColor.GREEN + "[WATCHBLOCK] Connection to sql was checked: Connected");
        } else if (!mysql.testConnection()) {
            Bukkit.getLogger().warning(ChatColor.DARK_RED + "[WATCHBLOCK] Connection to sql was checked: Disconnected");
            Bukkit.getLogger().warning(ChatColor.DARK_RED + "[WATCHBLOCK] Attempting to reconnect...");
            Bukkit.broadcastMessage(ChatColor.YELLOW + "[WATCHBLOCK] Watchblock database is attempting to reconnect, you may lag briefly...");
            try {
                mysql.connect();
                Bukkit.getLogger().info(ChatColor.GREEN + "[WATCHBLOCK] SQLAutoCheck reconnected the SQL database.");
                Bukkit.broadcastMessage(ChatColor.DARK_GREEN + "[WATCHBLOCK] Watchblock database reconnected.");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                Bukkit.getLogger().warning(ChatColor.DARK_RED + "[WATCHBLOCK] Something went wrong, SQLAutoCheck couldn't reconnect to database.");
            } catch (SQLException e) {
                Bukkit.getLogger().warning(ChatColor.DARK_RED + "[WATCHBLOCK] Something went wrong, SQLAutoCheck couldn't reconnect to database.");
                e.printStackTrace();
            }
        }
    }
}