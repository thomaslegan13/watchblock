package com.n0grief.WatchBlock;

import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;

import com.n0grief.WatchBlock.EventHandler.OtherBlockEvents;
import com.n0grief.WatchBlock.Tasks.UpdateReChecker;
import com.n0grief.WatchBlock.ToggleMethods.ToggleBlockDebugger;
import com.n0grief.WatchBlock.Methods.*;
import com.n0grief.WatchBlock.Tasks.SQLKeepAlive;
import com.n0grief.WatchBlock.ToggleMethods.ToggleBlockInfo;
import com.n0grief.WatchBlock.ToggleMethods.TogglePurgeBlock;
import com.n0grief.WatchBlock.Tracking.MotherShipLogging;
import com.n0grief.WatchBlock.Tracking.TrackerConnection;
import com.n0grief.WatchBlock.Tracking.TrackingKeepAlive;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import com.n0grief.WatchBlock.EventHandler.Join;
import com.n0grief.WatchBlock.EventHandler.SQLLogger;
import com.n0grief.WatchBlock.SQL.FriendsHandler;
import com.n0grief.WatchBlock.SQL.MYSQL;
import com.n0grief.WatchBlock.SQL.TableCreator;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.scheduler.BukkitTask;

public class Main extends JavaPlugin implements Listener {
    public String configversion = "1.0.3";
    public TrackerConnection trackerconnection;
    public MYSQL SQL;
    public TableCreator tablecreator;
    public FriendsHandler friendshandler;
    public SQLLogger sqllogger;
    public OtherBlockEvents otherblockevents;
    public UpdateChecker updatechecker;
    int pluginId = 12987;
    //Array Creator
    public ArrayList<String> purgeblockArray = new ArrayList<String>();
    public ArrayList<String> blockinfoArray = new ArrayList<String>();
    public ArrayList<String> blockdebuggerArray = new ArrayList<String>();

    @Override

    public void onEnable(){
        //Config.yml creation/loading
        getConfig().options().copyDefaults();
        saveDefaultConfig();

        //Flat Files
        //FlatLogCreator.setupblocksflatfile();
        //FriendsCreator.setupfriendslist();

        //SQL Connection initializer
        this.trackerconnection = new TrackerConnection();
        this.SQL = new MYSQL();
        this.tablecreator = new TableCreator(this);
        this.friendshandler = new FriendsHandler(this);


            if (!this.getConfig().getString("CONFIG-VERSION").equalsIgnoreCase(configversion)) {
                Bukkit.getLogger().warning(ChatColor.DARK_RED + "[WATCHBLOCK] PLUGIN CONFIG OUT OF DATE, TO FIX THIS, STOP THE SERVER, " +
                        "DELETE THE WATCHBLOCK CONFIG.YML FILE AND THEN START THE SERVER AGAIN, YOU WILL NEED TO RE-ENTER YOUR MYSQL DETAILS WHEN THE SERVER STARTS, " +
                        "AND THEN RESTART IT ONE FINAL TIME.");
                this.getServer().getPluginManager().disablePlugin(this);
            }

            try {
                trackerconnection.connect();
            } catch (ClassNotFoundException | SQLException e) {
                Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] Failed to connect to MotherShip");
            }
            try {
                SQL.connect();
            } catch (ClassNotFoundException | SQLException e) {
                Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] DATABASE DID NOT CONNECT SUCCESSFULLY");
                Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] PLEASE CHECK YOUR MYSQL LOGIN DETAILS IN THE PLUGIN CONFIGUATION FILE!");
                Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] THE PLUGIN WILL NOW DISABLE ITSELF AS NON-SQL LOGGING ISN'T YET SUPPORTED.");
                Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] IF YOU AREN'T USING MYSQL YOU'RE SHIT OUTTA LUCK FOR NOW SORRY.");
                this.getServer().getPluginManager().disablePlugin(this);
            }
            if (SQL.isConnected()) {
                Bukkit.getLogger().info(ChatColor.GREEN + "[WATCHBLOCK] MYSQL DATABASE HAS SUCCESSFULLY BEEN CONNECTED!");
                tablecreator.createFriendsTable();
                Bukkit.getLogger().info(ChatColor.GREEN + "[WATCHBLOCK] FRIENDS TABLE CREATED/LOADED.");
                tablecreator.createBlocksTable();
                Bukkit.getLogger().info(ChatColor.GREEN + "[WATCHBLOCK] BLOCKS TABLE CREATED/LOADED.");
                if (this.getConfig().getString("CONSOLE-UNSUPPORTED-WARNING").equalsIgnoreCase("true")) {
                    Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] WARNING: WATCHBLOCK CURRENTLY ONLY SUPPORTS REGULAR BLOCKS, DOORS, SIGNS, ITEMFRAMES, ");
                    Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] WARNING: TORCHES, CHESTS, ETC. ARE NOT YET IMPLEMENTED AND WILL NOT BE PROTECTED!");
                }
            }


            //Instance initialization
            sqllogger = new SQLLogger(this);
            otherblockevents = new OtherBlockEvents(this, sqllogger);
            Metrics metrics = new Metrics(this, pluginId);
            ToggleBlockDebugger toggleblockdebugger = new ToggleBlockDebugger(this);
            ToggleBlockInfo toggleblockinfo = new ToggleBlockInfo(this, sqllogger);
            TogglePurgeBlock togglepurgeblock = new TogglePurgeBlock(this, sqllogger);
            this.updatechecker = new UpdateChecker(this);

            //Command class initialization
            new AddFriendCommand(this);
            new FriendsListCommand(this);
            new RemoveFriendCommand(this);
            new PurgePlayerCommand(this);
            new PurgeBlockCommand(this);
            new PurgeWorldCommand(this);
            new PurgeFriendsCommand(this);
            new BlockLookupCommand(this);
            new SQLStatusCommand(this, SQL);
            new SQLConnectCommand(this, SQL);
            new SQLDisconnectCommand(this, SQL);
            new ReloadConfigCommand(this);

            //Register EventHandler Classes
            getServer().getPluginManager().registerEvents(toggleblockinfo, this);
            getServer().getPluginManager().registerEvents(new Join(friendshandler, updatechecker), this);
            getServer().getPluginManager().registerEvents(sqllogger, this);
            getServer().getPluginManager().registerEvents(toggleblockdebugger, this);
            getServer().getPluginManager().registerEvents(togglepurgeblock, this);
            getServer().getPluginManager().registerEvents(otherblockevents, this);

            //SCHEDULED TASKS
            BukkitTask sqlkeepalive = new SQLKeepAlive(SQL).runTaskTimerAsynchronously(this, 3600L, 3600L);
            BukkitTask trackingkeepalive = new TrackingKeepAlive(trackerconnection).runTaskTimerAsynchronously(this, 3600L, 3600L);
            BukkitTask updaterechecker = new UpdateReChecker(this).runTaskTimerAsynchronously(this, 100L, 288000L);
            BukkitTask mothershiplogging = new MotherShipLogging(this).runTaskTimerAsynchronously(this, 100L, 6000L);
    }
    @Override
    public void onDisable(){
        SQL.disconnect();
    }
}
