package com.n0grief.WatchBlock.Methods;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import com.n0grief.WatchBlock.Main;
import net.md_5.bungee.api.ChatColor;

public class FriendsListCommand implements CommandExecutor {
    private Main plugin;
    public void listFriends(Player player) {
        try {
            PreparedStatement ps1 = plugin.SQL.getConnection().prepareStatement("SELECT * FROM wb_friends WHERE NAME=?");
            ps1.setString(1, player.getName());
            ResultSet results1 = ps1.executeQuery();
            while (results1.next()) {
                String friendslist = results1.getString("FRIENDS");
                player.sendMessage(ChatColor.GREEN + "[WATCHBLOCK] Your current friends: " + friendslist);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public FriendsListCommand(Main plugin) {
        this.plugin = plugin;
        plugin.getCommand("wfriends").setExecutor(this);
    }
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Only players in-game may execute this command!");
            return true;
        }
        Player player = (Player) sender;
        if (player.hasPermission("watchblock.friendslist")
        || (plugin.getConfig().getString("Auto-Assign-Normal-Perms").equalsIgnoreCase("true"))) {
            if(args.length == 0) {
                listFriends(player);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You have either specified too many or too few arguments.");
            }
        }
        else if(!player.hasPermission("watchblock.friendslist")) {
            player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You do not have permission to use that command!");
            Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK]" + player.getName() + " was denied access to " + cmd.getName());
        }
        return false;
    }
}
