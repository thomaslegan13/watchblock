package com.n0grief.WatchBlock.Methods;

import com.n0grief.WatchBlock.Main;
import com.n0grief.WatchBlock.SQL.MYSQL;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.SQLException;

public class SQLConnectCommand implements CommandExecutor {
    private Main plugin;
    private final MYSQL mysql;
    public SQLConnectCommand(Main plugin , MYSQL mysql){
        this.mysql = mysql;
        plugin.getCommand("wsqlconnect").setExecutor(this);
    }
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        Player player = (Player) sender;
        if(player.hasPermission("watchblock.admin")){
            if(args.length == 0) {
                if(!mysql.testConnection()) {
                    Bukkit.broadcastMessage(ChatColor.DARK_RED + "[WATCHBLOCK] Attempting to connect to SQL server...");
                    try {
                        mysql.connect();
                        Bukkit.broadcastMessage(ChatColor.DARK_GREEN + "[WATCHBLOCK] The SQL database has been manually reconnected!");
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                        Bukkit.broadcastMessage(ChatColor.RED + "[WATCHBLOCK] Unable to connect to SQL server.");
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                        Bukkit.broadcastMessage(ChatColor.RED + "[WATCHBLOCK] Unable to connect to SQL server.");
                    }
                    return true;
                }
                else if(mysql.testConnection()){
                    Bukkit.broadcastMessage(ChatColor.RED + "[WATCHBLOCK] The SQL server is already connected.");
                    return true;
                }
            }
            else{
                sender.sendMessage(ChatColor.RED + "[WATCHBLOCK] You have specified too many arguments.");
                return false;
            }
        }
        else {
            player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You do not have permission to use that command!");
            Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] " + player.getName() + " was denied access to " + cmd.getName());
            return true;
        }
        return false;
    }
}
