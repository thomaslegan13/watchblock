package com.n0grief.WatchBlock.Tracking;

import com.n0grief.WatchBlock.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MotherShipLogging extends BukkitRunnable {
    private Main plugin;
    public MotherShipLogging(Main plugin) {
        this.plugin = plugin;
    }

    private String serverip = getIP();
    private int port = Bukkit.getPort();
    public String serverVersion = Bukkit.getVersion();
    public boolean isWhitelisted = Bukkit.getServer().hasWhitelist();
    public boolean isOnlineMode = Bukkit.getOnlineMode();
    public String sevrerMOTD = Bukkit.getMotd();


    public String getformattedTime() {
        LocalDateTime unformattedTime = LocalDateTime.now();
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm:ss");
        String formattedTime = unformattedTime.format(timeFormatter);
        return formattedTime;
    }

    public String getIP() {
        try {
            HttpsURLConnection connection = (HttpsURLConnection) new URL("https://icanhazip.com").openConnection();
            connection.setRequestMethod("GET");
            String serverIP = new BufferedReader(new InputStreamReader(connection.getInputStream())).readLine();
            return serverIP;
        } catch (IOException e) {
            Bukkit.getLogger().info("[WATCHBLOCK] There was an issue getting your IP.");
            return null;
        }
    }

    public boolean serverExists(String serverip, int port) {
        int serverPort = port;
        String serverIp = serverip;
        try {
            PreparedStatement ps2 = plugin.trackerconnection.getConnection().prepareStatement("SELECT * FROM servers WHERE IP=? AND PORT=?");
            ps2.setString(1, serverip);
            ps2.setInt(2, port);
            ResultSet results1 = ps2.executeQuery();
            if (results1.next()) {
                return true;
            } else return false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void run() {
        String formattedTime = getformattedTime();
        if (!serverExists(serverip, port)) {
            try {
                PreparedStatement ps1 = plugin.trackerconnection.getConnection().prepareStatement("INSERT INTO servers" + "(IP,PORT) VALUES(?,?)");
                ps1.setString(1, serverip);
                ps1.setInt(2, port);
                ps1.executeUpdate();
                Bukkit.getLogger().info(ChatColor.GREEN + "[WATCHBLOCK] Server was added to MotherShip list.");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        String thisServer = " WHERE `IP`='" + serverip + "' AND `PORT`=" + port;
        try {
            PreparedStatement ps3 = plugin.trackerconnection.getConnection().prepareStatement("UPDATE servers SET `LAST-SEEN`=?" + thisServer);
            ps3.setString(1, formattedTime);
            ps3.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try{
            PreparedStatement ps4 = plugin.trackerconnection.getConnection().prepareStatement("UPDATE servers SET `PLUGIN-VERSION`=?" + thisServer);
            ps4.setString(1 , plugin.getDescription().getVersion());
            ps4.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
        try{
            PreparedStatement ps5 = plugin.trackerconnection.getConnection().prepareStatement("UPDATE servers SET `SERVER-VERSION`=?" + thisServer);
            ps5.setString(1 , serverVersion);
            ps5.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        try{
            PreparedStatement ps6 = plugin.trackerconnection.getConnection().prepareStatement("UPDATE servers SET `ONLINE-MODE`=?" + thisServer);
            ps6.setBoolean(1 , isOnlineMode);
            ps6.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        try{
            PreparedStatement ps7 = plugin.trackerconnection.getConnection().prepareStatement("UPDATE servers SET `WHITELIST`=?" + thisServer);
            ps7.setBoolean(1 , isWhitelisted);
            ps7.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        try{
            PreparedStatement ps8 = plugin.trackerconnection.getConnection().prepareStatement("UPDATE servers SET `MOTD`=?" + thisServer);
            ps8.setString(1 , sevrerMOTD);
            ps8.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        try{
            PreparedStatement ps9 = plugin.trackerconnection.getConnection().prepareStatement("UPDATE servers SET `PLAYERS`=?" + thisServer);
            ps9.setInt(1 , plugin.getServer().getOnlinePlayers().size());
            ps9.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        try{
            PreparedStatement ps10 = plugin.trackerconnection.getConnection().prepareStatement("UPDATE servers SET `CONFIG-VERSION`=?" + thisServer);
            ps10.setString(1 , plugin.getConfig().getString("CONFIG-VERSION"));
            ps10.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}