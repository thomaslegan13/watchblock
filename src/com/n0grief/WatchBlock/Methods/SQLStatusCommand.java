package com.n0grief.WatchBlock.Methods;

import com.n0grief.WatchBlock.Main;
import com.n0grief.WatchBlock.SQL.MYSQL;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.SQLException;

public class SQLStatusCommand implements CommandExecutor {
    private Main plugin;
    private final MYSQL mysql;
    public SQLStatusCommand(Main plugin , MYSQL mysql){
        this.mysql = mysql;
        plugin.getCommand("wsqlcheck").setExecutor(this);
    }
    public boolean wsqlStatus(Player player, String args) {
        if (mysql.testConnection()) {
            return true;
        }
        else {
            return false;
        }
    }
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        Player player = (Player) sender;
        if(!(sender instanceof Player)){
            Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] You must be in-game to use this command.");
            return true;
        }
        if(player.hasPermission("watchblock.admin")){
            if(args.length == 0) {
                if(!mysql.testConnection()){
                    player.sendMessage(ChatColor.DARK_RED + "[WATCHBLOCK] SQL SERVER IS DISCONNECTED!");
                    return true;
                }
                else if(mysql.testConnection()){
                    player.sendMessage(ChatColor.GREEN + "[WATCHBLOCK] The SQL Server is connected.");
                    return true;
                }
            }
            else{
                player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You have specified too many arguments.");
                return false;
            }
        }
        else {
            player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You do not have permission to use that command!");
            Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] " + player.getName() + " was denied access to " + cmd.getName());
            return true;
        }
        return false;
    }
}
