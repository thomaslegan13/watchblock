package com.n0grief.WatchBlock.Tasks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.bukkit.Bukkit;

import com.n0grief.WatchBlock.Main;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;


//Method by n0_grief aka TH3_GR13F

public class UpdateReChecker extends BukkitRunnable {
    private Main plugin;
    public UpdateReChecker(Main plugin) {
        this.plugin = plugin;
    }

    public void run(){
        isUpdated();
    }
    public boolean isUpdated() {
        String url = "https://api.spigotmc.org/legacy/update.php?resource=";
        String id = "96521&bust" + System.currentTimeMillis();
        Bukkit.getLogger().info(ChatColor.YELLOW + "[WATCHBLOCK] Checking for updates...");
        try {
            String localVersion = plugin.getDescription().getVersion();
            HttpsURLConnection connection = (HttpsURLConnection) new URL(url + id).openConnection();
            connection.setRequestMethod("GET");
            String remoteVersion = new BufferedReader(new InputStreamReader(connection.getInputStream())).readLine();
            if(localVersion.equalsIgnoreCase(remoteVersion)) {
                Bukkit.getLogger().info(ChatColor.GREEN + "[WATCHBLOCK] No new version found!");
                Bukkit.getLogger().info(ChatColor.GREEN + "[WATCHBLOCK] You're running version " + localVersion + " Latest: " + remoteVersion);
                return true;
            }
            else{
                Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] A newer version of watchblock was found on spigot!");
                Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] Please update watchblock at https://www.spigotmc.org/resources/watchblock.96521/");
                Bukkit.getLogger().info(ChatColor.YELLOW + "[WATCHBLOCK] You are running Version " + localVersion + " Latest Version= "  + remoteVersion);
                return false;
            }
        }catch (IOException e) {
            Bukkit.getLogger().warning(ChatColor.DARK_RED + "An error occurred when checking for updates, check your internet connection!");
            //e.printStackTrace();
            return false;
        }
    }

}
