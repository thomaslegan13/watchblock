package com.n0grief.WatchBlock.Methods;

import com.n0grief.WatchBlock.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class ReloadConfigCommand implements CommandExecutor {
    private Main plugin;
    public ReloadConfigCommand(Main plugin){
        this.plugin = plugin;
        plugin.getCommand("wreload").setExecutor(this);
    }
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(args.length == 0) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                if (player.hasPermission("watchblock.reload")) {
                    plugin.reloadConfig();
                    player.sendMessage(ChatColor.GREEN + "[WATCHBLOCK] Configuration reloaded.");
                } else {
                    player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You do not have permission to use that command!");
                    Bukkit.getLogger().info("[WATCHBLOCK] " + player.getName() + " was denied access to " + cmd.getName());
                }
            } else if (sender instanceof ConsoleCommandSender) {
                Bukkit.getLogger().info("[WATCHBLOCK] Config reloaded");
            }
            return true;
        }
        return false;
    }
}
